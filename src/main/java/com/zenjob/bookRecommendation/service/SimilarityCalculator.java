package com.zenjob.bookRecommendation.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zenjob.bookRecommendation.domain.Books;
import com.zenjob.bookRecommendation.domain.Users;
import com.zenjob.bookRecommendation.repository.BooksRepository;

@Service
public class SimilarityCalculator {

	private static final int SIMILARITY_THRESHOLD = 50;

	private BooksRepository booksRepositroy;

	@Autowired
	public SimilarityCalculator(BooksRepository booksRepository) {
		this.booksRepositroy = booksRepository;
	}

	public List<Books> recommendFor(Users user) {
		Map<Books, Integer> recommended = new HashMap<>();
		for (Books b : booksRepositroy.findAll()) {
			Integer similarityCoeff = calculateSimilarity(b, user);
			if (similarityCoeff > 0 && similarityCoeff < SIMILARITY_THRESHOLD) {
				recommended.put(b, similarityCoeff);
			}
		}
		Stream<Entry<Books, Integer>> sorted = recommended.entrySet().stream().sorted(Entry.comparingByValue());
		return sorted.map(Entry::getKey).collect(Collectors.toList());
	}

	public Integer calculateSimilarity(Books currentBook, Users user) {
		String currentBookString = new StringBuilder().append(currentBook.getTitle()).append(currentBook.getAuthor())
				.append(currentBook.getGenre()).toString();

		List<Books> allLikedBooks = user.getLikedBooks();
		allLikedBooks.remove(currentBook);
		Integer similarityWithPositives = calculateSimilarity(allLikedBooks, user, currentBookString);

		List<Books> allBooksDislikedByUser = user.getDislikedBooks();
		allBooksDislikedByUser.remove(currentBook);
		Integer similarityWithNegatives = calculateSimilarity(allBooksDislikedByUser, user, currentBookString);

		return similarityWithPositives - similarityWithNegatives;
	}

	private Integer calculateSimilarity(List<Books> books, Users user, String bTBCStringBuilder) {
		Map<Books, Integer> distancesMapPositives = calculateDistancesMap(bTBCStringBuilder, books);
		Integer similarityWithPositives = distancesMapPositives.entrySet().stream()
				.collect(Collectors.summingInt(Entry::getValue));
		return similarityWithPositives;
	}

	private Map<Books, Integer> calculateDistancesMap(String currentBook, List<Books> allBooksLikedByUser) {
		Map<Books, Integer> distancesMap = new HashMap<>();
		for (Books b : allBooksLikedByUser) {
			String bStringBuilder = new StringBuilder().append(b.getTitle()).append(b.getAuthor()).append(b.getGenre())
					.toString();

			Integer distance = new LevenshteinDistance().apply(currentBook, bStringBuilder);
			distancesMap.put(b, distance);
		}
		return distancesMap;
	}
}
