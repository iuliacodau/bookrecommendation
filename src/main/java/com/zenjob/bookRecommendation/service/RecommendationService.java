package com.zenjob.bookRecommendation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zenjob.bookRecommendation.domain.Books;
import com.zenjob.bookRecommendation.domain.Users;
import com.zenjob.bookRecommendation.repository.BooksRepository;
import com.zenjob.bookRecommendation.repository.UsersRepository;

@Service
public class RecommendationService {

	private UsersRepository usersRepository;

	private BooksRepository booksRepository;

	private SimilarityCalculator similarityCalculator;

	@Autowired
	public RecommendationService(UsersRepository userRepository, BooksRepository bookRepository,
			SimilarityCalculator similarityCalculator) {
		this.usersRepository = userRepository;
		this.booksRepository = bookRepository;
		this.similarityCalculator = similarityCalculator;
	}

	public List<Books> getRecommendationsFor(String username) {
		List<Books> recommendedBooks = new ArrayList<>();

		Users currentUser = usersRepository.findByUsername(username).orElseThrow(() -> new MongoException("The User with username " + username + " doesn't exist"));

		List<Books> recommendFor = similarityCalculator.recommendFor(currentUser);
		recommendedBooks.addAll(recommendFor);
		if (recommendedBooks.size() >= 20) {
			return recommendedBooks.subList(0, 20);
		}

		List<Books> recommendedByGenre = getBooksRecommendedByGenre(currentUser);
		recommendedBooks.addAll(recommendedByGenre);

		if (recommendedBooks.size() < 20) {
			List<Books> recommendedByAuthor = getBooksRecommendedByAuthor(currentUser);
			recommendedBooks.addAll(recommendedByAuthor);
		}
		
		return recommendedBooks.size() > 20 ? recommendedBooks.subList(0, 20) : recommendedBooks;
	}

	private List<Books> getBooksRecommendedByAuthor(Users currentUser) {
		String favouriteAuthor = currentUser.getFavouriteAuthor();
		List<Books> favByAuthor = booksRepository.findByAuthor(favouriteAuthor);
		return favByAuthor;
	}

	private List<Books> getBooksRecommendedByGenre(Users currentUser) {
		String favouriteGenre = currentUser.getFavouriteGenre();
		List<Books> recommendedByGenre = booksRepository.findByGenre(favouriteGenre);
		return recommendedByGenre;
	}

}
