package com.zenjob.bookRecommendation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zenjob.bookRecommendation.domain.Feedback;
import com.zenjob.bookRecommendation.domain.Users;
import com.zenjob.bookRecommendation.repository.UsersRepository;

@Service
public class FeedbackService {

	@Autowired
	private UsersRepository userRepository;

	public void addFeedbacksForUser(String username, List<Feedback> feedbacks) {
		Optional<Users> withFeedback = userRepository.findByUsername(username);
		withFeedback.ifPresent(usr -> usr.addFeedback(feedbacks));
		userRepository.save(withFeedback.orElseThrow(() -> new MongoException("The User with username " + username + " doesn't exist")));
	}
}
