package com.zenjob.bookRecommendation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zenjob.bookRecommendation.domain.Books;
import com.zenjob.bookRecommendation.domain.Feedback;
import com.zenjob.bookRecommendation.service.FeedbackService;
import com.zenjob.bookRecommendation.service.RecommendationService;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	private RecommendationService recommendationService;
	@Autowired
	private FeedbackService feedbackService;

	@RequestMapping(value = "/recommendations/{username}", method = RequestMethod.GET)
	public List<Books> getRecommendedBooks(@PathVariable("username") String username) {
		return recommendationService.getRecommendationsFor(username);
	}

	@RequestMapping(value = "/feedbacks/{username}", method = RequestMethod.PUT)
	public void addFeedback(@PathVariable("username") String username, @RequestBody List<Feedback> feedbacks) {
		feedbackService.addFeedbacksForUser(username, feedbacks);
	}

}
