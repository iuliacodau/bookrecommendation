package com.zenjob.bookRecommendation.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.zenjob.bookRecommendation.domain.Books;

public interface BooksRepository extends MongoRepository<Books, String>{
	
	List<Books> findByAuthor(String author);
	
	List<Books> findByGenre(String genre);
}
