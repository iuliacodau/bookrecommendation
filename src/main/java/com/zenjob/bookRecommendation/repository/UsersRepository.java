package com.zenjob.bookRecommendation.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.zenjob.bookRecommendation.domain.Users;

public interface UsersRepository extends MongoRepository<Users, String>{

	Optional<Users> findByUsername(String id);

}
