package com.zenjob.bookRecommendation.jobs;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.zenjob.bookRecommendation.domain.Books;

@Configuration
@EnableBatchProcessing
public class InsertBooksJob {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Bean
	public Job readBooksDataset() {
		return jobBuilderFactory.get("readBooksDataset").incrementer(new RunIdIncrementer()).start(booksDatasetStep())
				.build();
	}

	@Bean
	public Step booksDatasetStep() {
		return stepBuilderFactory.get("booksDatasetStep").<Books, Books>chunk(100).reader(booksDatasetReader())
				.writer(booksDatasetWriter()).build();
	}

	@Bean
	public FlatFileItemReader<Books> booksDatasetReader() {
		FlatFileItemReader<Books> reader = new FlatFileItemReader<>();
		reader.setResource(new ClassPathResource("booksDataset"));
		reader.setLineMapper(new DefaultLineMapper<Books>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer(";") {
					{
						setNames(new String[] { "AISIN", "Title", "Author", "Genre" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Books>() {
					{
						setTargetType(Books.class);
					}
				});
			}
		});
		return reader;
	}

	@Bean
	public MongoItemWriter<Books> booksDatasetWriter() {
		MongoItemWriter<Books> writer = new MongoItemWriter<Books>();
		writer.setTemplate(mongoTemplate);
		writer.setCollection("books");
		return writer;
	}
}
