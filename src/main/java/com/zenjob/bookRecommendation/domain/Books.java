package com.zenjob.bookRecommendation.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="books")
public class Books {

	@Id
	private String aisin;
	private String author;
	private String title;
	private String genre;

	public Books() {
		
	}
	
	public Books(String aisin, String author, String title, String genre) {
		this.aisin = aisin;
		this.author = author;
		this.title = title;
		this.genre = genre;
	}
	
	public String getAisin() {
		return aisin;
	}

	public void setAisin(String aisin) {
		this.aisin = aisin;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

}
