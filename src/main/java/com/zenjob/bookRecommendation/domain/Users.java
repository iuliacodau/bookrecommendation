package com.zenjob.bookRecommendation.domain;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Comparator.reverseOrder;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class Users {

	private static final String UNDEFINED = "Undefined";
	@Id
	private String id;
	@Indexed
	private String username;
	private List<Feedback> feedbacks = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public void addFeedback(List<Feedback> feedbacks) {
		this.feedbacks.addAll(feedbacks);
	}

	public List<Books> getLikedBooks() {
		return feedbacks.stream().filter(f -> TRUE.equals(f.isPositive())).map(Feedback::getBook)
				.collect(Collectors.toList());
	}

	public List<Books> getDislikedBooks() {
		return feedbacks.stream().filter(f -> FALSE.equals(f.isPositive())).map(Feedback::getBook)
				.collect(Collectors.toList());
	}

	public String getFavouriteGenre() {
		List<String> likedBooks = getLikedBooks().stream().map(Books::getGenre).collect(Collectors.toList());
		if (!likedBooks.isEmpty()) {
			return likedBooks.stream().collect(toMap(w -> w, w -> 1, Integer::sum)).entrySet().stream()
					.sorted(comparingByValue(reverseOrder())).collect(toList()).get(0).getKey();
		}
		return UNDEFINED;
	}

	public String getFavouriteAuthor() {
		List<String> likedBooks = getLikedBooks().stream().map(Books::getAuthor).collect(Collectors.toList());
		if (!likedBooks.isEmpty()) {
			return likedBooks.stream().collect(toMap(w -> w, w -> 1, Integer::sum)).entrySet().stream()
					.sorted(comparingByValue(reverseOrder())).collect(toList()).get(0).getKey();
		}
		return UNDEFINED;

	}

}
