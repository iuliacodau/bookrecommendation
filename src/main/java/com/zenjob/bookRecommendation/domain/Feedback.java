package com.zenjob.bookRecommendation.domain;

public class Feedback {

	private Boolean isPositive;
	private Books book;

	public Boolean isPositive() {
		return isPositive;
	}

	public void setIsPositive(Boolean value) {
		this.isPositive = value;
	}

	public Books getBook() {
		return this.book;
	}

	public void setBook(Books book) {
		this.book = book;
	}

}
