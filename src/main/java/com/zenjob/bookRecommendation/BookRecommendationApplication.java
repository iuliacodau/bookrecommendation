package com.zenjob.bookRecommendation;

import org.bson.Document;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

@SpringBootApplication
public class BookRecommendationApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(BookRecommendationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		MongoClient mongo = new MongoClient("localhost", 27017);
		MongoDatabase db = mongo.getDatabase("bookRecomm");
		MongoCollection<Document> usersCollection = db.getCollection("users");

		//test users for the application
		insertIfNotExists(usersCollection, "john");
		insertIfNotExists(usersCollection, "jane");
		
		//users for the BookControllerTest
		insertIfNotExists(usersCollection, "iulia");
		insertIfNotExists(usersCollection, "jack");

		Bson filter = eq("username", "jack");
		Document feedbacks = new Document();
		feedbacks.put("isPositive", true);

		Document book = new Document();

		book.put("aisin", "60515236");
		book.put("author", "Neil Gaiman");
		book.put("title", "Fragile Things: Short Fictions and Wonders");
		book.put("genre", "Literature & Fiction");

		feedbacks.put("book", book);
		Bson change = push("feedbacks", feedbacks);
		usersCollection.updateOne(filter, change);

		mongo.close();
	}

	private void insertIfNotExists(MongoCollection<Document> usersCollection, String username) {
		Document user = usersCollection.find(eq("username", username)).first();
		if (user == null) {
			user = new Document("username", username);
			usersCollection.insertOne(user);
		}
	}

}
