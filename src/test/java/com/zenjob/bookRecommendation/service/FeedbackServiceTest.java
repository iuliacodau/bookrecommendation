package com.zenjob.bookRecommendation.service;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import com.mongodb.MongoException;
import com.zenjob.bookRecommendation.domain.Feedback;
import com.zenjob.bookRecommendation.domain.Users;
import com.zenjob.bookRecommendation.repository.UsersRepository;

public class FeedbackServiceTest {

	private static final String DUMMY_USER = "Dummy";

	@Rule
	public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@Mock
	private UsersRepository usersRepository;

	@InjectMocks
	private FeedbackService objectUnderTest;

	@Test
	public void addFeedbacksForUser_notInDb_throwsException() {
		when(this.usersRepository.findByUsername(DUMMY_USER)).thenReturn(Optional.ofNullable(null));

		Throwable thrown = catchThrowable(
				() -> objectUnderTest.addFeedbacksForUser(DUMMY_USER, Arrays.asList(new Feedback())));
		assertThat(thrown).isInstanceOf(MongoException.class)
				.hasMessage("The User with username " + DUMMY_USER + " doesn't exist");
	}

	@Test
	public void addFeedbacksForUser_success() {
		Users user = new Users();
		when(this.usersRepository.findByUsername(DUMMY_USER)).thenReturn(Optional.of(user));

		assertTrue(user.getFeedbacks().isEmpty());
		this.objectUnderTest.addFeedbacksForUser(DUMMY_USER, Arrays.asList(new Feedback()));

		assertEquals(user.getFeedbacks().size(), 1);
	}

}
