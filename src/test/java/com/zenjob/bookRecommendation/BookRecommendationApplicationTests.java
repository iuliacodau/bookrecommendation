package com.zenjob.bookRecommendation;

import static org.junit.Assert.*;

import javax.validation.constraints.AssertTrue;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zenjob.bookRecommendation.repository.BooksRepository;
import com.zenjob.bookRecommendation.repository.UsersRepository;
import com.zenjob.bookRecommendation.service.RecommendationService;
import com.zenjob.bookRecommendation.service.SimilarityCalculator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRecommendationApplicationTests {

	private RecommendationService serviceToTest ;
	@Mock
	private BooksRepository bookRepo;
	@Mock
	private UsersRepository userRepo;
	@Mock 
	private SimilarityCalculator similarity;
	
	@Before
	public void setUp() {
		serviceToTest = new RecommendationService(userRepo, bookRepo, similarity);
		
	}
	
	@Test
	public void dummy() {
		String a = new String("Three Wishes: A NovelLiane MoriartyLiterature & Fiction");
		String b = new String("Big Little LiesLiane MoriartyMystery, Thriller & Suspense");
		Integer dist = new LevenshteinDistance().apply(a, b);
		
		assertTrue(dist>0);
	}
	

}
