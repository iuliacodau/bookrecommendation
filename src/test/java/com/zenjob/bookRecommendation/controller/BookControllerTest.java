package com.zenjob.bookRecommendation.controller;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {

	private static final String TEST_USER_IULIA = "iulia";
	private static final String TEST_USER_JACK = "jack";
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getRecommendations_withoutFeedbacks_returnsEmpty() throws Exception {
		//Jill has no feedbacks yet therefore we cannot recommend anything
		mockMvc.perform(MockMvcRequestBuilders.get("/books/recommendations/" + TEST_USER_IULIA)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));
	}
	
	@Test
	public void getRecommendations_returnsBooks() throws Exception {
		//Jack has a liked book so we can recommend him a list of other 20 books
		mockMvc.perform(MockMvcRequestBuilders.get("/books/recommendations/" + TEST_USER_JACK)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(20)));
	}
}
